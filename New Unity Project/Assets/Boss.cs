﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public Transform Point;
    public GameObject virus;
    public float rate = .01f;
    float nextAttack = 0f;

    void Update()
    {
        if (Time.time >= nextAttack)
        {
            Spawn();
            nextAttack = Time.time + 1f / rate;
        }
    }

    void Spawn()
    {
        GameObject obj = Instantiate(virus, Point.position + new Vector3(0, 2, 0), Point.rotation);
    }
}
