﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    public Transform point;
    public float range = 0.07f;
    public LayerMask eLayer;
    public LayerMask bLayer;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Collider2D hit = Physics2D.OverlapCircle(point.position, range, eLayer);
        if (hit != null)
            hit.GetComponent<Follow>().TakeDamage();
        Destroy(gameObject);
    }
}
