﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Transform Point;
    public GameObject bullet;
    public float force = 10f;

    void Update()
    {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Fire("u");
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Fire("d");
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Fire("l");
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                Fire("r");
            }
    }

    void Fire(string direct)
    {
        switch (direct) {
            case "u":
                GameObject obj = Instantiate(bullet, Point.position + new Vector3(0, 1, 0), Point.rotation);
                Rigidbody2D rb = obj.GetComponent<Rigidbody2D>();
                rb.AddForce(Point.up * force, ForceMode2D.Impulse);
                break;
            case "d":
                obj = Instantiate(bullet, Point.position + new Vector3(0, -1, 0), Point.rotation);
                rb = obj.GetComponent<Rigidbody2D>();
                rb.AddForce(Point.up * -force, ForceMode2D.Impulse);
                break;
            case "l":
                obj = Instantiate(bullet, Point.position + new Vector3(-1, 0, 0), Point.rotation);
                rb = obj.GetComponent<Rigidbody2D>();
                rb.AddForce(Point.right * -force, ForceMode2D.Impulse);
                break;
            case "r":
                obj = Instantiate(bullet, Point.position + new Vector3(1, 0, 0), Point.rotation);
                rb = obj.GetComponent<Rigidbody2D>();
                rb.AddForce(Point.right * force, ForceMode2D.Impulse);
                break;
        }
    }
}


