﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{

    public int maxHealth = 10;
    public int currHealth;
    public float speed = 5f;
    public Rigidbody2D rb;
    Vector2 move;
    public GameObject e1, e2, e3;

    void Start()
    {
        currHealth = maxHealth;
    }

    void Update()
    {
        if(currHealth <= 0) 
        {
            GetComponent<Collider2D>().enabled = false;
            this.enabled = false;
        }
        move.x = Input.GetAxisRaw("Horizontal");
        move.y = Input.GetAxisRaw("Vertical");
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + move * speed * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == e1 || collision.gameObject == e2 || collision.gameObject == e3)
            currHealth -= 1;
    }
}


