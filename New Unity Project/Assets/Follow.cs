﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public int maxH = 8;
    public int currentH;
    public float speed = 1f;
    public Transform target;
    public Rigidbody2D rb;
    public Transform point;
    public float range = 3f;
    public LayerMask pLayer;

    private void Start()
    {
        currentH = maxH;
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void Update()
    {
        if (currentH <= 0)
        {
            GetComponent<Collider2D>().enabled = false;
            this.enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
        }
        Collider2D hit = Physics2D.OverlapCircle(point.position, range, pLayer);
        if (currentH != maxH || hit != null)
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        rb.velocity = new Vector2(0, 0);
    }

    public void TakeDamage() 
    {
        currentH -= 4;
    }
}
